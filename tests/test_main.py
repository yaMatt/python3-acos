import unittest
from acos import Acos

class TestWithStatement(unittest.TestCase):
    SAMPLE_URL="memory://"

    def test_with(self):
        SAMPLE_KEY="key"
        SAMPLE_VALUE="value"
        with Acos.from_url(self.SAMPLE_URL, instant=True) as acos:
            acos.set_string(SAMPLE_KEY, SAMPLE_VALUE)
            self.assertEqual(acos.get_string(SAMPLE_KEY), SAMPLE_VALUE)
