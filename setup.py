#!/usr/bin/env python3

from setuptools import setup

setup(
    name='Acos',
    version='1.0',
    description='Abstraction layer for cloud object storage',
    author='Matt Copperwaite',
    author_email='matt@copperwaite.net',
    url='https://git.copperwaite.net/matt/python3-acos',
    packages=[
        'acos',
    ],
    test_suite = 'tests',
)
