from .services import service_generator

class Acos(object):
    @classmethod
    def from_url(cls, url, *args, **kwargs):
        """
        A short description.

        A bit longer description.

        Args:
            variable (type): description

        Returns:
            type: description

        Raises:
            Exception: description

        """

        return cls(
            service_generator(url),
            *args, **kwargs
        )

    def __init__(self, service, instant=False):
        """
        A short description.

        A bit longer description.

        Args:
            variable (type): description

        Returns:
            type: description

        Raises:
            Exception: description

        """

        self.service = service
        self.instant = instant

    def get(self, key):
        """
        A short description.

        A bit longer description.

        Args:
            variable (type): description

        Returns:
            type: description

        Raises:
            Exception: description

        """

        return self.get_string(key)

    def get_string(self, key):
        return self.service.get(key)

    def get_file(self, key):
        pass

    def set(self, key, object):
        if hasattr(object, "read"):
            return self.set_file(key, object)
        return self.set_string(key, object)

    def set_string(self, key, value):
        self.service.set(key, value)

    def set_file(self, key, file_object):
        pass

    def __getitem__(self, key):
        return self.get(key)

    def __setitem__(self, key, value):
        self.set(key, value)

    def __len__(self):
        pass

    def __str__(self):
        return self.service.url

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.service.close()
