from .s3 import *

NAME="Digital Ocean Spaces"
URL="https://www.digitalocean.com/products/spaces/"
HELP="The API attempts to replicate AWS S3 storage."
