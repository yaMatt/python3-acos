from urllib.parse import urlparse
from . import memory, dir, s3, b2, dospaces, scalewayos

SERVICES_SCHEME_MAP = {
    memory.SCHEME: memory,
    dir.SCHEME: dir,
    s3.SCHEME: s3,
    b2.SCHEME: b2,
    dospaces.SCHEME: dospaces,
    scalewayos.SCHEME: scalewayos,
}

def get_service_from_url(url):
    service_name = urlparse(url).scheme
    return SERVICES_SCHEME_MAP[service_name]

def service_generator(url):
    return get_service_from_url(url).SERVICE.from_url(url)
