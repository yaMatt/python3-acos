class BaseService(object):
    @classmethod
    def from_url(cls, url, *args, **kwargs):
        return cls(
            url,
            *args, **kwargs
        )

    def close(self):
        pass
