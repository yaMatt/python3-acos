from .base import BaseService

class Memory(BaseService):

    def __init__(self, url, *args, **kwargs):
        self.url = url
        self._memory = {}

    def upload(self, key):
        return self._memory[key]

    def download(self, key, value):
        self._memory[key] = value

    def close(self):
        del self._memory

SERVICE = Memory
SCHEME = "memory"
NAME="Local memory storage"
URL=""
HELP="Stores your objects in RAM. For testing and development purposes only."
