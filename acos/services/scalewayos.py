from .s3 import *

NAME="Scaleway Object Storage"
URL="https://www.scaleway.com/object-storage/"
HELP="The API attempts to replicate AWS S3 storage."
